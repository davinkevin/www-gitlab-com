---
layout: markdown_page
title: "Technical Writing Team"
---

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

## About Us

The primary goal of the Technical Writing team is to continuously develop GitLab's
product documentation content to meet the evolving needs of all users and administrators.
The documentation is intended to educate readers about features and best practices,
and to enable them to efficiently configure, use, and troubleshoot GitLab. To this end, the
team also manages the docs.gitlab.com site and related process and tooling.

Technical writers partner with anyone in the GitLab community who is concerned with
documentation, especially developers, who are the first to update docs for the
GitLab features that they code.

For more information on documentation at GitLab, see the [Documentation](../../documentation/)
section of the Handbook.

## Responsibilities

- [**Documentation Content**](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Documentation&label_name[]=epic-level-1&search=Content)
    - [Developing new content](https://gitlab.com/groups/gitlab-org/-/epics/774) to meet the needs of the community.
    - [Reviewing and collaborating on documentation plans](https://gitlab.com/groups/gitlab-org/-/epics/776), reviewing doc merge requests or recently merged docs, and ensuring that content meets style and language standards.
    - [Reorganizing, revamping, and authoring improved content](https://gitlab.com/groups/gitlab-org/-/epics/775) to ensure completeness and a smooth user experience.

- [**Documentation Site**](https://gitlab.com/groups/gitlab-com/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Documentation) (docs.gitlab.com)
    - Maintaining and enhancing the documentation site’s architecture, design, automation, versioning, search, SEO, feedback methods, and analytics.

- [**Documentation Process**](https://gitlab.com/groups/gitlab-org/-/epics/779)
    - Ensuring that processes are in place and being followed to keep the GitLab docs up to date.
    - Following and optimizing documentation workflows with Product and Engineering, Documentation team workflows, and the division of work.
    - Triaging doc-related issues.
    - Refining the Documentation Style Guide and continuously improving content about GitLab documentation and its contribution process.
    - Making it easier for anyone to contribute to the documentation while efficiently handling community contributions to docs.

- [**Collaboration**](https://gitlab.com/groups/gitlab-org/-/epics/779)
    - Working on documentation efforts with Product, Support, Marketing, Engineering, Community Marketing, other GitLab teams, and the wider GitLab community.
    - Ensuring that relevant documentation is easily accessible from within the product.
    - Acting as reviewers of the monthly [release post](../../handbook/marketing/blog/release-posts/).

This work is sorted into the [top-level Documentation epics](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Documentation&label_name%5B%5D=epic-level-1&scope=all&sort=created_asc&state=opened&utf8=%E2%9C%93)
linked above.

## FY20 Vision

- [**New Content Development**](https://gitlab.com/groups/gitlab-org/-/epics/774) - Leading the planning and authoring of new content to better meet user needs. This will be a significantly increased proportion of the team’s work, and will make use of improved methods and standards.
  - Authoring docs that better address use cases with A-to-Z process flows, going beyond existing feature references and procedures that can be difficult to adapt to real-world use cases. Supported by [updated standards for content types and structure](https://gitlab.com/gitlab-com/Product/issues/74).
  - [Getting Started](https://gitlab.com/groups/gitlab-org/-/epics/782) - a.k.a. unboxing or onboarding docs for various audiences and scenarios.
- [**Content Improvement**](https://gitlab.com/groups/gitlab-org/-/epics/775)
  - Revamping existing content pages/sections, so that they meet our latest standards, including contextual information on every page (what, why, who).
  - Improve use of documentation feedback/sensing mechanisms. Improved sourcing and channeling/triage; making use of existing comments and issues, while bringing in new sources including search data, surveys, user testing, etc.
  - [Improve rate of contributions to docs as SSOT](https://gitlab.com/gitlab-com/Product/issues/51) and using ‘docs-first’ methods by tracking contributions and demonstrating best practices.
- **Docs UX**
  - Next iteration of [docs versioning](https://gitlab.com/gitlab-com/gitlab-docs/issues/248).
  - [Removing documentation from /help](https://gitlab.com/groups/gitlab-org/-/epics/693), linking instead from UI to versioned links on docs.gitlab.com.

## Documentation standards and workflows

For information on creating documentation at GitLab, see our the following pages in the handbook:

- [Documentation](../../documentation/).
- [Technical Writing workflow](workflow/).

We also maintain [contributor documentation](https://docs.gitlab.com/ee/development/documentation).
