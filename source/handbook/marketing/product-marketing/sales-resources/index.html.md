---
layout: markdown_page
title: "Sales Resources"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Field Resources
The purpose of this page is to be a quick reference to help the sales team navigate and find resources to help them be more efficient and effective.  If you find a broken link - either **update it** or **contact the PMM team**.  Ultimately, these resources should ALL be available on our main [resources page](https://about.gitlab.com/resources/). For now, this is a work in progress resource. **Sales Team**: if you have comments, suggestions, or feedback about this page, this [issue- #366](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/366) is open for ongoing feedback.

### Pricing and tiers

* [Why Ultimate](/pricing/ultimate/)
* [Why Premium](/pricing/premium/)
* [Self-manged pricing](/pricing/#self-managed)
* [GitLab.com pricing](/pricing/#gitlab-com)

### Presentations

* [Pitch Deck:](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing) - The primary GitLab overview presentation.
* [Professional Services Deck](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit?usp=sharing)
* [Security Overview](https://docs.google.com/presentation/d/1z4v6v_lP7BHCP2jfRJ9bK_XoUgQ9XW01X2ZhQcon8bY/edit#slide=id.g2823c3f9ca_0_9)
* [Accelerating Delivery Short Talk](https://drive.google.com/open?id=15PgY9Dm0emVlWZdBjJX15nVxDR47l1MwhOF-tGbzY5Y)
* [Kubernetes 101](https://docs.google.com/presentation/d/1fWjiVgSNMKTHyC6_nWDY5rDhvdm-zEQMQytZysIXAzk/edit)
* [Digital & Cloud Native Transformation (GitLab Benefits) Deck](https://docs.google.com/presentation/d/1mcg5E4dztQZQTN4WvJ4sdqnRGZ6Wz7IRDa8_o8-p4vg/edit#slide=id.g51da45aefc_0_5)
* [Cloud Native Transformation (vendor agnostic speaker deck)](https://docs.google.com/presentation/d/15FN3tebw8LZMYkOMtqaPmHV17NDVZy0K6Tou1-TS-Ac/edit#)

### GitLab by the Stages

Key resources by stage.

#### Manage

* [Geo and DR](https://about.gitlab.com/solutions/geo/)
* [High Availability](https://about.gitlab.com/solutions/high-availability/)
* [Value Stream Management](https://about.gitlab.com/solutions/value-stream-management/)

#### Plan

* [Agile Delivery Overview](https://about.gitlab.com/solutions/agile-delivery/)
* [Agile Teams Video](https://youtu.be/VR2r1TJCDew)
* [Gitlab and Scaled Agile Framework Video](https://www.youtube.com/watch?v=PmFFlTH2DQk&feature=youtu.be)

#### Create

* [SCM Overview](https://about.gitlab.com/product/source-code-management/)

#### Secure
* [Dev Sec Ops Overview](https://about.gitlab.com/solutions/dev-sec-ops/)
* [Security Demo Video](https://about.gitlab.com/resources/video-gitlab-security-demo/)
* [Whitepaper GitLab App Sec Workflow](https://about.gitlab.com/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)

#### Verify, Package, Release, Configure

Customers aren't going to think about or ask for different "stages". They are going to say things like
- We want to adopt DevOps
- We want to adopt CI/CD
- We want to adopt Kubernetes (same thing as saying "We want to go cloud native")

##### CI/CD Resources

* [CI/CD overview](/product/continuous-integration/)
* [Scalable app deployment with Google Cloud and Gitlab video](https://www.youtube.com/watch?v=uWC2QKv15mk) (Great Kuberentes 101 content for customers that are unfamilar. Also Demos Auto DevOps.)
* [Automating Kubernetes Deployments video](https://www.youtube.com/watch?v=wEDRfAz6_Uw) (For folks familiar with Kubernetes or Cloud Native. This is a webinar hosted by the CNCF. Goes into both business value and more technical detail on GitLab CI/CD)
* [GitLab + Kubernetes](/solutions/kubernetes/)
* [What is Cloud Native?](/cloud-native/)
* [What are microservices?](/topics/microservices/), [Business value of microservices](/topics/microservices/#business-value-of-microservices)
* [What is serverless?](/topics/serverless/), [Business value of serverless](https://about.gitlab.com/topics/serverless/#business-value-of-serverless)

#### Monitor

* tbd

#### Defend

* tbd

### [Analyst Reports](https://about.gitlab.com/analysts/)

- [Overview: Forrester Continuous Integration Tools](https://about.gitlab.com/analysts/forrester-ci/) and [Report: GitLab and The Forrester Wave™: Continuous Integration Tools, Q3 2017](https://about.gitlab.com/resources/forrester-wave-ci-2017/)
- [Overview: Forrester Value Stream Management](https://about.gitlab.com/analysts/forrester-vsm/) and [Report: GitLab and The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/resources/forrester-new-wave-vsm-2018/)
- [Overview: Gartner Application Release Orchestration](https://about.gitlab.com/analysts/gartner-aro/) and [Report: GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018](https://about.gitlab.com/resources/forrester-wave-ci-2017/)
- [Overview: IDC Innovators Agile Code Development Technologies, 2018](https://about.gitlab.com/analysts/idc-innovators/) and [Report: IDC Innovators Agile Code Development Technologies, 2018](https://page.gitlab.com/rs/194-VVC-221/images/IDC-Innovators-Agile-Code-Development-Technologies-2018.pdf)

- [Analyst Program Overvew Page](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/)


### White Papers

Key Whitepapers:

- [A seismic shift in application security](https://about.gitlab.com/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)
- [Bridging the divide between developers and management ](https://about.gitlab.com/resources/downloads/201806_WP_Bridging_developers_and_management.pdf)
- [What is Concurrent DevOps](https://about.gitlab.com/resources/downloads/gitlab-concurrent-devops-whitepaper.pdf)
- [*Scaled CI and CD*](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-scaled-ci-cd-whitepaper.pdf)
- [*Moving to Git: a guide*](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-moving-to-git-whitepaper.pdf)
- [How GiLab is Enterprise Class](https://about.gitlab.com/solutions/enterprise-class/)
- [2018 Developer Report](https://about.gitlab.com/developer-survey/2018/)

### Datasheets

- [GitLab Capabilities](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)
- [GitLab Data Sheet](https://about.gitlab.com/images/press/gitlab-data-sheet.pdf)

### [Customer References](https://about.gitlab.com/customers/)

- [Goldman Sachs improves from two daily builds to over a thousand per day](https://about.gitlab.com/customers/goldman-sachs/)
- [How Wag! cut their release process from 40 minutes to just 6 minutes](https://about.gitlab.com/2019/01/16/wag-labs-blog-post/)
- [Axway realizes a 26x faster release cycle by switching from Subversion to GitLab](https://about.gitlab.com/customers/axway/)
- [Paessler AG switches from Jenkins and ramps up to 4x more releases](https://about.gitlab.com/customers/paessler/)
- [Trek10 provides radical visibility to clients](https://about.gitlab.com/customers/trek10/)
- [Particle physics laboratory uses GitLab to connect researchers from across the globe](https://about.gitlab.com/customers/cern/)
- [iFarm plants the seeds for operational efficiency](https://about.gitlab.com/customers/ifarm/)
- [Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions](https://about.gitlab.com/customers/european-space-agency/)
- [How GitLab CI supported Ticketmaster’s ramp up to weekly mobile releases](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/)
- [The Cloud Native Computing Foundation eliminates complexity with a unified CI/CD system](https://about.gitlab.com/customers/cncf/)
- [Worldline hosts 14,500 projects and has 3,000 active users on their GitLab platform](https://about.gitlab.com/customers/worldline/)
- [Equinix increases the agility of their DevOps teams with self-serviceability and automation](https://about.gitlab.com/customers/equinix/)
- [The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects](https://about.gitlab.com/customers/uw/)

- [Overall Customer Reference Program](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/)


### [Comparisons](https://about.gitlab.com/devops-tools/)

Here are links to a few **KEY comparisons** and then a link to main comparison page where over 50 comparisons are available.
- [GitHub](https://about.gitlab.com/devops-tools/github-vs-gitlab.html)
- [BitBucket](https://about.gitlab.com/devops-tools/bitbucket-vs-gitlab.html)
- [AWS Codestar](https://about.gitlab.com/devops-tools/codestar-vs-gitlab.html)
- [Jenkins](https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html)
- [Azure](https://about.gitlab.com/devops-tools/azure-devops-vs-gitlab.html)
- [Jira](https://about.gitlab.com/devops-tools/jira-vs-gitlab.html)

### Demos
- [Demo Videos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/#videos)
- [Demo Click Throughs](https://about.gitlab.com/handbook/marketing/product-marketing/demo/#click-throughs)
- [Demo Instructions](https://about.gitlab.com/handbook/marketing/product-marketing/demo/#live-instructions)


### Getting Started with GitLab  (Services)
- [Professional Services](https://about.gitlab.com/services/)
- [Quick Start Implementation Package](https://about.gitlab.com/services/implementation/quickstart)
- [Dedicated Implementation Planning](https://about.gitlab.com/services/implementation/enterprise)
- [GitLab Training](https://about.gitlab.com/services/education)

### ROI - ?

The ROI calculator is a work in progress - welcome contributions.
- [Overall ROI page](https://about.gitlab.com/roi/)  
- [GitLab replacing other tools](https://about.gitlab.com/roi/replace/)


### Enablement

- [Sales Enablement Youtube](https://www.youtube.com/watch?v=ZyyBq3_rzJo&list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG)
- [Sales Enablement Handbook](https://about.gitlab.com/handbook/marketing/product-marketing/enablement/)
