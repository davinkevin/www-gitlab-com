---
layout: markdown_page
title: "Test Engineering Process"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Quality Engineering department helps facilitate the test planning process for all things related to Engineering work.

## Child Pages

#### [Test Design Heuristics](./test-design)

### Test Plan

* The test plan is essentially the test requirement linked to the product requirement.
* The test plan is the place to plan Quality and Risk management on how to achieve optimal test coverage across all test levels when delivering a change/feature.
* The output or deliverable of the test plans are:
  * Requirements on unit test coverage.
  * Requirements on integration test coverage.
  * Requirements on End-to-end test scenarios (where applicable).
    * A test plan does not always need to result in new end-to-end tests. If there are adequate unit and integration tests then its acceptable to close out the plan.
  * One-off manual testing as needed (ideally this should be at a minimal and only required for adhoc/exploratory testing).


GitLab Quality Test plan is based on [Google’s 10 min test plan](https://testing.googleblog.com/2011/09/10-minute-test-plan.html).
This team plan uses the ACC Framework (Attribute, Components and Capabilities matrix)
* Attributes: qualities the product should have
* Components: major parts of the product
* Capabilities: behavior the product should display that links components and attributes

Currently we have a [test plan template](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab/issue_templates/Test%20plan.md) that is available for test planning in GitLab CE & EE.  

You can see the all the existing test plans here:
* [GitLab CE Test Plans](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=test%20plan)
* [GitLab EE Test Plans](https://gitlab.com/gitlab-org/gitlab-ee/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=test%20plan)

#### Security

In addition to Quality, Security is also our top concern. It is expected that with every change and every new feature we need to factor in security risks and mitigate them early in the development process.

The test plan template has a risk column dedicated to Security. The following should be the top priority while assessing risk in Security.

* Exposure of user confidential information.
* Exposure of access; authentication, token.

### Test Planning Process

* At release kickoff test plans are created with a rough mapping to the new features being developed from a stage group. 
  * A test plan can be created by any engineer in the stage group.
  * A test plan can have a one-to-one mapping to an issue/epic or a plan can be used to cover every item for a stage group. This depends on the suitability and size of the work.
* The test strategy discussions happen with the purpose to come up with clear test deliverables for tests at different levels.
* Engineers in a given stage group are responsible for completing the test deliverables.
* Engineers from Quality Engineering ensure that we complete the test coverage as planned before the feature is merged into `master` and released to production.
  * If a feature requires an End-to-End test, the Quality / Test Automation Engineer adds `Requires e2e tests` label to the feature MR.
  * Before merging the feature MR, the developer or Quality / Test Automation Engineer ensures the e2e test MR is linked to the feature MR and checks off "Link to e2e tests MR added if this issue/MR has requires e2e tests label". 
* Once all test deliverables are completed, the test plan can be closed.  

Everyone in engineering is expected to contribute to Quality and keep our test pyramid in top shape.
For every new feature we aim to ship a new slice of the pyramid so we don't incur test automation debt.
This is what enables us to do Continuous Delivery.

![TestPyramid.png](TestPyramid.png)


### Test Coverage Tracking

We are currently evaluating using GitLab mechanics to track test coverage in [gitlab-org/quality/testcases](https://gitlab.com/gitlab-org/quality/testcases/issues).

Our previous iteration can be seen at [DEPRECATED - GitLab Blackbox Integration & E2E Test Coverage](https://docs.google.com/spreadsheets/d/1RlLfXGboJmNVIPP9jgFV5sXIACGfdcFq1tKd7xnlb74/edit).

An explanation of why we chose to do it this way is explained in this issue: [Test case management and test tracking in a Native Continuous Delivery way](https://gitlab.com/gitlab-org/gitlab-ce/issues/51790).

To summarize, we want to track our tests in a Native Continuous Delivery way.
* If we are doing continuous delivery in the right way, there should be little to no manual tests. Manual tests if any should be minimal and exploratory.
* There should be little need for detailed test steps if manual testing is minimal and exploratory.
  * Exploratory testing has a free form emphasis which removes the importance of hardcoded test steps.
  * The disadvantage of going into too much detail with hardcoded test steps is we are not able to catch bugs outside the hardcoded flow in the document, it's also a high maintenance document.
  * Our test case name / description contains just enough description, this helps fuzzes the workflow up and we end up catching more critical bugs this way.
* An emphasis on risk-based test planning using the ACC framework.
* An emphasis on categorizing test automation types which tracks the pyramid shape (API, UI, and Visual).
* An emphasis on tracking alignment in the lower test levels in the pyramid.
* An emphasis on intelligent test design using [Test Design heuristics](test-design).

An overview of the test management view is shown below.

![TestCaseManagement.png](TestCaseManagement.png)
