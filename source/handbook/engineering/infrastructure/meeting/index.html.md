---
layout: markdown_page
title: "Meetings"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

| **Workflow** | [**How may we be of service?**](production/#workflow--how-we-work) | **GitLab.com Status** | [**`STATUS`**](https://status.gitlab.com/)
| **Issue Trackers** | [**Infrastructure**](https://gitlab.com/gitlab-com/infrastructure/issues/): [Milestones](https://gitlab.com/gitlab-com/infrastructure/milestones), [OnCall](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=oncall) | [**Production**](https://gitlab.com/gitlab-com/production/issues/): [Incidents](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=incident), [Changes](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=change), [Deltas](https://gitlab.com/gitlab-com/production/issues?label_name%5B%5D=delta)  | [**Delivery**](https://gitlab.com/gitlab-org/release/framework)
| **Slack Channels** | [#sre-lounge](https://gitlab.slack.com/archives/sre-lounge), [#database](https://gitlab.slack.com/archives/database) | [#alerts](https://gitlab.slack.com/archives/alerts), [#production](https://gitlab.slack.com/archives/production) | [#g_delivery](https://gitlab.slack.com/archives/g_delivery)
| **Operations** | [Runbooks](https://gitlab.com/gitlab-com/runbooks) (please contribute!) | **On-call**: [Handover Document](https://docs.google.com/document/d/1IrTi06fUMgxqDCDRD4-e7SJNPvxhFML22jf-3pdz_TI), [Reports](https://gitlab.com/gitlab-com/infrastructure/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=oncall%20report) |

## Meetings

GitLab is a widely distributed company, and we aim to work asynchronously most of the time. There are times, however, when we must get *together* to discuss a topic in real time, and thus, we do have some meetings schedule.

### Infrastructure Call (InfraCall)

The Infrastructure Call is one of many channels to share and distribute Infrastructure-related information relevant to the entire company and beyond. The meeting is organized by the Director of Infrastructure.

#### Agenda

The InfraCall agenda is divided into the following sections:

* **General Updates**: compact, timely and relevant notifications (new teams members, OKR updates, etc)
* **Handbook Updates**: highlights from Handbook updates in Infrastructure, especially those that affect workflows and that are useful to other teams, groups and department across the company.
* **Production Updates**: a summary of production-related topics
  * **Incidents**: S1 and S2 incidents over the last 7 days
  * **Deltas**: current deltas in production, and updates on progress to remove them
  * **Changes**: upcoming changes to production over the next 30 days (from change management)
* **Agenda**: additional agenda items for discussion

#### Logistics

Infrastructure managers and ICs update relevant sections before the meeting. Other attendees are welcome to add agenda items as necessary.

### Design and Automation (DNA)

Design and Automation (DNA) is a **purely technical meeting** for Infrastructure ICs to discuss technical topics. Managers also attend this meeting, but they take off their manager hat as soon as they join the meeting. The meeting is organized by the Director of Infrastructure.

#### Agenda

The DNA agenda is driven by [blueprints](../blueprint/) and [designs](../design/); open discussions are also welcome as long as they are technically relevant. Project status discussions are strictly out of bounds in this  (the only exception being the resolution of technical dependencies).

#### Logistics

While open discussions are welcome, it is strongly recommended blueprints and designs are used as the source of agenda items. This allows everyone to have the appropriate context for the conversation before the meeting.

During discussions, it is obviously ok to point shortcomings for a given design. This is one way in which we expand our angle of vision and learn. In general, however, **make it a point to provide alternatives**.

### Staff Meeting

Each team in Infrastructure has a weekly Staff meeting, where relevent team issues are discussed. These meetinfs are organized by Infrastructure Managers for their respective teams.

#### Agenda

*Flesh out*

### mStaff Meeting

The weekly mStaff brings together Infrastructure's management team for a weekly sync early in the week to prepare for the week and address issues that require attention. The meeing is organized by the Director of Infrastructure.

*Flesh out*